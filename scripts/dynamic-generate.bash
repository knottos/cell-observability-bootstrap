#!/usr/bin/env bash

set -euo pipefail

# This is a generic script for generating the raw prom rules, it is designed to be used with the `k8s-dynamic` provider
# Either transplanted to a job, or run adhoc within the container.

if [ -z "$RUNBOOKS_REPO_LOCAL" ]; then
    echo "RUNBOOKS_REPO_LOCAL is not set, refusing to evaluate"
    exit 1
fi
if [ -z "$RUNBOOKS_REPO_REMOTE" ]; then
    echo "RUNBOOKS_REPO_REMOTE is not set, refusing to evaluate"
    exit 1
fi
if [ -z "$RUNBOOKS_REPO_TAG" ]; then
    echo "RUNBOOKS_REPO_TAG is not set, refusing to evaluate"
    exit 1
fi
if [ -z "$RUNBOOKS_OUTPUT_DIR" ]; then
    echo "RUNBOOKS_OUTPUT_DIR is not set, refusing to evaluate"
    exit 1
fi

if [ ! -d "$RUNBOOKS_REPO_LOCAL" ]; then
  git clone "$RUNBOOKS_REPO_REMOTE" "$RUNBOOKS_REPO_LOCAL"
  (cd "$RUNBOOKS_REPO_LOCAL" && git checkout "$RUNBOOKS_REPO_TAG") 
else
  (cd "$RUNBOOKS_REPO_LOCAL" && git pull origin "$RUNBOOKS_REPO_TAG")
fi

cd "$RUNBOOKS_REPO_LOCAL"

jb install

# Generate baseline metrics configuration for reference architecture
"$RUNBOOKS_REPO_LOCAL"/scripts/generate-reference-architecture-config.sh "$RUNBOOKS_REPO_LOCAL"/reference-architectures/get-hybrid/src/ "$RUNBOOKS_OUTPUT_DIR"/metrics-catalog/config
if [ "$K8SREMOTE_CALLBACK_ADDR" == "" ]; then
  exit 0
fi

# Presume we're running remotely if this is set, in which case we need to post our results back to this url.
#shellcheck disable=SC2044
for f in $(find "$RUNBOOKS_OUTPUT_DIR" -iname "*.yml"); do 
  # Get the name from the yaml file
  FILENAME="$(basename "$f" .yml)"
  curl -XPOST -H "Content-Type: text/yaml" --data-binary "@$f" "$K8SREMOTE_CALLBACK_ADDR?name=$FILENAME" -v
done
