#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
MODULE_ROOT="$(cd "$SCRIPT_DIR"/.. && pwd)"

if [ -z "$NIX_SHELL_DIR" ]; then
  NIX_SHELL_DIR="$MODULE_ROOT/.nix-shell"
fi

KUBECONFIG="$NIX_SHELL_DIR/.kube/config"
KUBE_CONFIG_PATH="$KUBECONFIG"
KIND_CLUSTER_NAME="knottos-obs-bootstrap"

# If the .nix-shell directory already exists then just continue as normal, assume
# that this is a second shell and the kind cluster is already provisioned
if [ -d "$NIX_SHELL_DIR" ]; then
  echo "$NIX_SHELL_DIR present already, assuming the kind cluster has already been provisioned."
  exit 0
fi

# Just do a quick check to see if weve got access to the docker daemon
# If not then just skip doing anything.
if ! docker ps >/dev/null 2>&1; then
  echo "Docker daemon not found or access missing, skipping shellHook.."
  exit 0
fi

mkdir -p "$NIX_SHELL_DIR"

# Build a kind cluster
cat <<EOF >"$NIX_SHELL_DIR"/kind.config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: $KIND_CLUSTER_NAME
EOF

echo "Spinning up a k8s cluster"
kind create cluster \
  --config "$NIX_SHELL_DIR/kind.config"

kind export kubeconfig \
  --name $KIND_CLUSTER_NAME \
  --kubeconfig $KUBECONFIG \

echo "Adhoc installing the prometheus operator, which need in order to test this project!"
kubectl create -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/main/bundle.yaml

cat <<EOF
Temporary Environment written to $NIX_SHELL_DIR/env

Useful things to do next:
EOF

cat <<EOF >"$NIX_SHELL_DIR"/env
export KUBECONFIG=$KUBECONFIG
export KUBE_CONFIG_PATH=$KUBE_CONFIG_PATH
export KIND_CLUSTER_NAME=$KIND_CLUSTER_NAME
export NIX_SHELL_DIR=$NIX_SHELL_DIR
EOF

echo "Sourcing environment from $NIX_SHELL_DIR/env"
# shellcheck disable=SC1091
source "$NIX_SHELL_DIR/env"