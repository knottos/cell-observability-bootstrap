# Cell-Observability-Bootstrap

Bootstraps dynamic observability infrastructure for cells, a companion project for https://gitlab.com/gitlab-com/gl-infra/terraform-modules/observability/cells

# Development

```bash
# Start a shell to hack in
$ nix develop

# Hack hack hack
$ vim cmd/cbs/main.go

# Usual development workflow
$ go run cmd/cbs/main.go
$ go build cmd/cbs/main.go
```


```bash
# Build+Run the binary
$ nix run '.#bin'

# Run some e2e tests
$ nix run '.#e2etests'

# Build+Run the docker container
$ nix run '.#container'
```

# Architecture
![img](https://gitlab.com/knottos/cell-observability-bootstrap/-/raw/main/docs/arch.png)