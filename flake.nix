{
  description = "cell-observability-bootstrap";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/24.05";
    flake-utils.url = "github:numtide/flake-utils";

    systems = {
      url = "github:nix-systems/default";
      flake = false;
    };
  };
  outputs = {self, nixpkgs, systems, ...}: 
    let
      forEachSystem = nixpkgs.lib.genAttrs (import systems);
    in
    {
      packages = forEachSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };

          jsonnet-tool = (pkgs.buildGoModule rec {
            pname = "jsonnet-tool";
            version = "1.9.1";
            src = pkgs.fetchFromGitLab {
              owner = "gitlab-com/gl-infra";
              repo  = "jsonnet-tool";
              rev   = "v${version}";
              sha256 = "sha256-zcBK+bxphhS9Fb7xluKIMZyWRLX4QfgzVAazxOo50nw=";
            };
            vendorHash = "sha256-7qdd9VtGPtPlmArWBYXC2eTAAaLM0xAXUxbt7NCyQVU=";
          });
          generate-script-remote = pkgs.writeScriptBin "generate-script" (builtins.readFile ./scripts/dynamic-generate.bash);

          bin = (pkgs.callPackage ./default.nix {
            buildGoModule = pkgs.buildGoModule;
            lib           = pkgs.lib;
          });
          container = pkgs.dockerTools.buildLayeredImage {
            name = "cell-obs-bootstrap";
            contents = with pkgs.dockerTools; [
              usrBinEnv
              binSh
              caCertificates
              fakeNss
              pkgs.bash
              pkgs.coreutils


              # Runtimes inputs 
              pkgs.git
              pkgs.jsonnet-bundler
              pkgs.go-jsonnet
              pkgs.curl
              pkgs.findutils
              pkgs.gawk
              jsonnet-tool
            ];
            config = {
              Env = [
                "K8SDYNAMIC_GENERATION_SCRIPT_PATH=${generate-script-remote}/bin/generate-script"
                "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
              ];
              Cmd = ["${bin}/bin/cbs"];
            };
          };

          e2etests = pkgs.writeShellApplication{
            name = "cells-bootstrap-e2e-tests";
            runtimeInputs = [
              pkgs.go
              pkgs.git
              pkgs.jsonnet-bundler
              jsonnet-tool
              pkgs.gnumake
              pkgs.go-jsonnet
              pkgs.kind
              pkgs.kubectl
            ];
            text = ''
              NIX_SHELL_DIR="$(mktemp -ud)"
              export NIX_SHELL_DIR

              echo "Emphermal nix-dir: $NIX_SHELL_DIR"
              echo "Booting dev environment.."
              ${./scripts/dev-up.sh}

              # shellcheck source=/dev/null
              source "$NIX_SHELL_DIR"/env

              # shellcheck disable=SC2064
              trap \
                "
                # shellcheck disable=SC2064
                NIX_SHELL_DIR=$NIX_SHELL_DIR ./scripts/dev-down.sh
                " \
                EXIT  

              echo "Container -> Building.."
              tag=$(docker load -qi ${container} | awk -F: '{print $3}')
              echo "Container -> cell-obs-bootstrap:$tag"
              echo "Container -> Loading.."
              kind load docker-image cell-obs-bootstrap:"$tag"
              echo "Container -> Done!"

              kubectl create ns monitoring

              while ! kubectl -n monitoring get sa default > /dev/null 2>&1; do
                sleep 1
              done

              cat << EOF | kubectl create -f - 
                apiVersion: rbac.authorization.k8s.io/v1
                kind: RoleBinding
                metadata:
                  namespace: monitoring
                  name: monitoring
                subjects:
                - kind: ServiceAccount
                  name: default
                  namespace: monitoring
                roleRef:
                  kind: ClusterRole
                  name: cluster-admin
                  apiGroup: rbac.authorization.k8s.io
              EOF

              i=1
              #shellcheck disable=SC2043
              for p in "K8SDYNAMIC_REMOTE"; do
                echo "Testing Provider -> $p"
                kubectl -n monitoring run e2ecbs-$i \
                  --rm=true \
                  --image=cell-obs-bootstrap:"$tag" \
                  --expose=true \
                  --port=9003 \
                  --attach=true \
                  --stdin=true \
                  --tty=true \
                  --env="PROVIDER=$p" \
                  --env="K8SDYNAMIC_RUNBOOKS_LOCAL=/mnt/runbooks" \
                  --env="K8SDYNAMIC_OUTPUT_DIR=/tmp/output" \
                  --env="K8SDYNAMIC_GENERATION_SCRIPT_PATH=${generate-script-remote}/bin/generate-script" \
                  --env="RUNBOOKS_REMOTE=https://gitlab.com/gitlab-com/runbooks.git" \
                  --env="RUNBOOKS_TAG=master" \
                  --env="TARGET_NAMESPACE=monitoring" \
                  --env="K8SDYNAMIC_CALLBACK_ADDR=http://e2ecbs-$i:9003/k8sdynamic_remote"

                kubectl -n monitoring delete prometheusrules --all
                i=$((i+1))
              done


              exit 0
            '';
          };
        in
        {
          container = container;
          bin       = bin;
          e2etests  = e2etests;
          generate-script = generate-script-remote;
        });
      devShells = forEachSystem
      (system: 
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };

          jsonnet-tool = (pkgs.buildGoModule rec {
            pname = "jsonnet-tool";
            version = "1.9.1";
            src = pkgs.fetchFromGitLab {
              owner = "gitlab-com/gl-infra";
              repo  = "jsonnet-tool";
              rev   = "v${version}";
              sha256 = "sha256-zcBK+bxphhS9Fb7xluKIMZyWRLX4QfgzVAazxOo50nw=";
            };
            vendorHash = "sha256-7qdd9VtGPtPlmArWBYXC2eTAAaLM0xAXUxbt7NCyQVU=";
          });

        in
        {
          default = (pkgs.callPackage ./shell.nix {
            mkShell         = pkgs.mkShell;
            go              = pkgs.go;
            git             = pkgs.git;
            jsonnet-bundler = pkgs.jsonnet-bundler;
            jsonnet-tool    = jsonnet-tool;
            gnumake         = pkgs.gnumake;
            go-jsonnet      = pkgs.go-jsonnet;
            kind            = pkgs.kind;
            kubectl         = pkgs.kubectl;
          });
        });
    };

}


