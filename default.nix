{ 
  buildGoModule,
  lib
}:
let
in
buildGoModule rec {
  pname = "cell-observability-bootstrap";
  version = "1.0.0";
  src = lib.cleanSource ./.;

  vendorHash = "sha256-MqkZZvtJn+rxD358ztAGKjFPl+wKr7xA5DPtrYT/UG4=";
  
  meta = with lib; {
    description = "Bootstraps dynamic observability infrastructure for cells, a companion project for https://gitlab.com/gitlab-com/gl-infra/terraform-modules/observability/cells";
    homepage    = "https://gitlab.com/knottos/cell-observability-bootstrap";
    license     = licenses.mit;
    maintainers = [
      "@gitlab-org/scalability/observability"
    ];
  };
}
