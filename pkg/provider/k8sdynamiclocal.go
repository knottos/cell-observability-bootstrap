package provider

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"net/http"

	"github.com/prometheus/prometheus/model/rulefmt"

	v1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// K8sDynamic provider only accepts a given runbooks sha or tag, it then
// provisions the relevant components on-the-fly from that image/tag
type K8sDynamic struct {
	GenerationScriptPath string `yaml:"generation_script_path"`

	RunbooksTag    string `yaml:"runbooks_tag"`
	RunbooksRemote string `yaml:"runbooks_remote"`
	RunbooksLocal  string `yaml:"runbooks_local"`
	OutputDir      string `yaml:"output_dir"`

	// Local determines if we are running the bootstrapping process inside our own pod (as opposed to spinning up another pod for the job.)
	// This comes with certain disadvantages, such as tooling versions being inconsistent, but allows for easier testing and is a simpler implementation
	Local bool `yaml:"local"`

	// K8sClientSet for usage by the remote generate function.
	ClientSet    *kubernetes.Clientset `yaml:"clientset"`
	Namespace    string                `yaml:"namespace"`
	CallbackAddr string                `yaml:"callback_addr"`
}

func (k K8sDynamic) PromResources() (map[string]*rulefmt.RuleGroups, error) {
	if k.Local {
		return k.LocalGenerate()
	}
	return k.RemoteGenerate()
}

func (k *K8sDynamic) LocalGenerate() (map[string]*rulefmt.RuleGroups, error) {
	cmd := exec.Command(k.GenerationScriptPath)
	cmd.Env = []string{
		fmt.Sprintf("RUNBOOKS_REPO_LOCAL=%v", k.RunbooksLocal),
		fmt.Sprintf("RUNBOOKS_REPO_REMOTE=%v", k.RunbooksRemote),
		fmt.Sprintf("RUNBOOKS_REPO_TAG=%v", k.RunbooksTag),
		fmt.Sprintf("RUNBOOKS_OUTPUT_DIR=%v", k.OutputDir),
		fmt.Sprintf("K8SREMOTE_CALLBACK_ADDR=%v", ""),
		fmt.Sprintf("PATH=%v", os.Getenv("PATH")),
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		return map[string]*rulefmt.RuleGroups{}, errors.Join(errors.New(string(out)), err)
	}

	return k.GatherRules()
}

func (k *K8sDynamic) RemoteGenerate() (map[string]*rulefmt.RuleGroups, error) {
	ret := map[string]*rulefmt.RuleGroups{}

	b, err := os.ReadFile(k.GenerationScriptPath)
	if err != nil {
		return ret, err
	}
	// We need to replace the shebang here, as we're using a runbooks image.
	lines := strings.Split(string(b), "\n")
	lines[0] = "#!/bin/bash"

	script := strings.Join(lines, "\n")

	defer func() {
		// Clean up stuff afterwards.
		err = k.ClientSet.CoreV1().ConfigMaps(k.Namespace).Delete(context.Background(), "cbs-run-script", metav1.DeleteOptions{})
		if err != nil {
			log.Printf("Failed to clean up resources: configmaps/cbs-run-script; %v", err)
		}
		/*err = k.ClientSet.BatchV1().Jobs(k.Namespace).Delete(context.Background(), runJob.Name, metav1.DeleteOptions{})
		if err != nil {
			log.Printf("Failed to clean up resources: jobs/%v; %v", runJob.Name, err)
		}*/
	}()

	_, err = k.ClientSet.CoreV1().ConfigMaps(k.Namespace).Create(context.Background(), &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "cbs-run-script",
			Namespace: k.Namespace,
		},
		Data: map[string]string{
			"run": string(script),
		},
	}, metav1.CreateOptions{})
	if err != nil {
		return ret, err
	}

	mode := int32(0777)
	completions := int32(1)

	runJob, err := k.ClientSet.BatchV1().Jobs(k.Namespace).Create(context.Background(), &v1.Job{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: "cbs-rem-",
			Namespace:    k.Namespace,
		},
		Spec: v1.JobSpec{
			Completions: &completions,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					GenerateName: "cbs-rem-",
				},
				Spec: corev1.PodSpec{
					RestartPolicy: "Never",
					// We mount our configmap containing our run script here.
					Volumes: []corev1.Volume{
						{
							Name: "run-script",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									DefaultMode: &mode,
									LocalObjectReference: corev1.LocalObjectReference{
										Name: "cbs-run-script",
									},
								},
							},
						},
					},
					Containers: []corev1.Container{
						{
							Name:  "rb-generator",
							Image: "registry.gitlab.com/gitlab-com/runbooks:" + k.RunbooksTag,
							Env: []corev1.EnvVar{
								{
									Name:  "RUNBOOKS_REPO_LOCAL",
									Value: k.RunbooksLocal,
								},
								{
									Name:  "RUNBOOKS_REPO_REMOTE",
									Value: k.RunbooksRemote,
								},
								{
									Name:  "RUNBOOKS_REPO_TAG",
									Value: k.RunbooksTag,
								},
								{
									Name:  "RUNBOOKS_OUTPUT_DIR",
									Value: k.OutputDir,
								},
								{
									Name:  "K8SREMOTE_CALLBACK_ADDR",
									Value: k.CallbackAddr,
								},
							},
							Command: []string{
								"/mnt/run-script/run",
							},
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "run-script",
									MountPath: "/mnt/run-script",
								},
							},
						},
					},
				},
			},
		},
	}, metav1.CreateOptions{})
	if err != nil {
		return ret, err
	}

	wi, err := k.ClientSet.BatchV1().Jobs(k.Namespace).Watch(context.Background(), metav1.ListOptions{
		FieldSelector: "metadata.name=" + runJob.Name,
	})
	if err != nil {
		return ret, err
	}

	// Boot ourselves a http server so our job can callback to us
	http.HandleFunc("/k8sdynamic_remote", func(w http.ResponseWriter, r *http.Request) {
		// Listen for an incoming request, do the usual processing.
		// Add things to ret.
		if r.Method != "POST" {
			return
		}

		name := r.URL.Query().Get("name")
		if name == "" {
			// Refuse to parse anything with no name!
			return
		}

		log.Printf("Callback recieved for %v", name)

		defer r.Body.Close()

		b, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("Got error while reading callback body: %v", err)
			return
		}

		v, errs := rulefmt.Parse(b)
		if err != nil {
			log.Printf("Got error while handling callback body: %v", errors.Join(errs...))
		}

		ret[name] = v

	})

	go http.ListenAndServe(":9003", nil)

	for {
		e := <-wi.ResultChan()
		v, ok := e.Object.(*v1.Job)
		if !ok {
			log.Printf("Attempted to parse watch object as job and failed? Continuing to watch anyway. Object: %+v", e.Object)
			continue
		}

		// The job is finished.
		if v.Status.Succeeded > 0 {
			// If thats true, then we can totally safely return ret as it should've recieved it by now
			// im sure this totally wont come back to bite me later and is totally foolproof
			wi.Stop()
			return ret, nil
		}
	}
}

func (k *K8sDynamic) GatherRules() (map[string]*rulefmt.RuleGroups, error) {
	r := map[string]*rulefmt.RuleGroups{}

	// Recursively find all matches
	err := filepath.WalkDir(k.OutputDir, func(s string, d fs.DirEntry, e error) error {
		if filepath.Ext(s) == ".yml" {
			fileName := filepath.Base(s)
			resourceName := strings.Split(fileName, ".")[0]
			v, err := rulefmt.ParseFile(s)
			if err != nil {
				return errors.Join(err...)
			}
			r[resourceName] = v
		}
		return nil
	})
	return r, err
}
