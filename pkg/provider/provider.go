package provider

import "github.com/prometheus/prometheus/model/rulefmt"

type Provider interface {
	// Get all the prometheusresources from a given provider
	PromResources() (map[string]*rulefmt.RuleGroups, error)
}
