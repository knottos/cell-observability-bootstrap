package util

import (
	v1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	v1ac "github.com/prometheus-operator/prometheus-operator/pkg/client/applyconfiguration/monitoring/v1"
	"github.com/prometheus/prometheus/model/rulefmt"
	"k8s.io/apimachinery/pkg/util/intstr"
)

// Converts from prometheus types to k8s prometheusrule types
func RuleFmtToK8sPrometheusRule(i map[string]*rulefmt.RuleGroups, namespace string) ([]*v1ac.PrometheusRuleApplyConfiguration, error) {
	ret := []*v1ac.PrometheusRuleApplyConfiguration{}
	// Each set of groups
	for n, v := range i {
		groups := []v1ac.RuleGroupApplyConfiguration{}
		for _, g := range v.Groups {
			// Convert all the rules
			rules := []*v1ac.RuleApplyConfiguration{}
			for _, r := range g.Rules {
				expr := intstr.Parse(r.Expr.Value)
				durFor := v1.Duration(r.For.String())
				durKeepFor := v1.NonEmptyDuration(r.KeepFiringFor.String())
				rules = append(rules, &v1ac.RuleApplyConfiguration{
					Record:        &r.Record.Value,
					Alert:         &r.Alert.Value,
					Expr:          &expr,
					For:           &durFor,
					KeepFiringFor: &durKeepFor,
					Labels:        r.Labels,
					Annotations:   r.Annotations,
				})
			}

			interval := v1.Duration(g.Interval.String())
			groups = append(groups, *v1ac.RuleGroup().
				WithName(g.Name).
				WithInterval(interval).
				WithRules(rules...).
				WithLimit(g.Limit))
		}

		ret = append(ret, v1ac.PrometheusRule(n, namespace).
			WithSpec(&v1ac.PrometheusRuleSpecApplyConfiguration{
				Groups: groups,
			}))
	}

	return ret, nil
}
