# shell.nix
{ mkShell, 
  go,
  git,
  gnumake,
  jsonnet-tool,
  go-jsonnet,
  jsonnet-bundler, 
  kind,
  kubectl,
}:
let
in mkShell {
  name = "cell-observability-bootstrap";
  packages = [
    go
    git
    gnumake
    jsonnet-tool
    go-jsonnet
    jsonnet-bundler
    kind
    kubectl
  ];
  shellHook = ''
    echo "Brought to you with love from Scalability/Observability <3"

    wd=$(pwd)

    mkdir -p $wd/.dev/
    
    export PROVIDER=K8SDYNAMIC_LOCAL
    export K8SDYNAMIC_RUNBOOKS_LOCAL=$wd/.dev/runbooks
    export K8SDYNAMIC_OUTPUT_DIR=$wd/.dev/output
    export K8SDYNAMIC_GENERATION_SCRIPT_PATH=$(echo $(pwd)/scripts/dynamic-generate.bash)
    export RUNBOOKS_REMOTE=https://gitlab.com/gitlab-com/runbooks.git
    export RUNBOOKS_TAG=master
  ''; 
} 

