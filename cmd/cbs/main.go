package main

import (
	"context"
	"flag"
	"log"
	"os"
	"strconv"

	"gitlab.com/knottos/cell-observability-bootstrap/v2/pkg/provider"
	"gitlab.com/knottos/cell-observability-bootstrap/v2/pkg/util"

	pOperator "github.com/prometheus-operator/prometheus-operator/pkg/client/versioned"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func EnvOrDefault(e, d string) string {
	if os.Getenv(e) == "" {
		return d
	}
	return os.Getenv(e)
}

func BEnvOrDefault(e string, d bool) bool {
	if os.Getenv(e) != "" {
		return d
	}

	b, err := strconv.ParseBool(e)
	if err != nil {
		panic(err)
	}
	return b
}

func MkTempOrDie() string {
	err := os.Mkdir("/tmp", os.ModeDir)
	d, err := os.MkdirTemp("/tmp", "")
	if err != nil {
		panic(err)
	}

	return d
}

var (
	TargetNamespace = flag.String("target-namespace", EnvOrDefault("TARGET_NAMESPACE", "default"), "namespace to deploy rules in.")
	Provider        = flag.String("provider", EnvOrDefault("PROVIDER", "K8SDYNAMIC_LOCAL"), "Provider to use for fetching rules")

	// Flags specific to the k8sDynamic provider
	k8sDynamic_Runbooks_Local       = flag.String("k8sdynamic.runbooks-local", EnvOrDefault("K8SDYNAMIC_RUNBOOKS_LOCAL", "/mnt/runbooks"), "Local runbooks path for the k8sdynamic provider")
	k8sDynamic_OutputDir            = flag.String("k8sdynamic.output-dir", EnvOrDefault("K8SDYNAMIC_OUTPUT_DIR", MkTempOrDie()), "Local runbooks path for the k8sdynamic provider")
	k8sDynamic_GenerationScriptPath = flag.String("k8sdynamic.generation-script-path", EnvOrDefault("K8SDYNAMIC_GENERATION_SCRIPT_PATH", "/scripts/dynamic-generate.bash"), "Dynamic generation script for the k8sdynamic provider")
	K8sDynamic_Remote_CallbackAddr  = flag.String("k8sdynamic.remote.callbackaddr", EnvOrDefault("K8SDYNAMIC_CALLBACK_ADDR", "http://localhost:9003/k8sdynamic_remote"), "Callback address for the remote k8sdynamic generator.")

	RunbooksRemote = flag.String("runbooks-remote", EnvOrDefault("RUNBOOKS_REMOTE", "https://gitlab.com/gitlab-com/runbooks.git"), "Remote runbooks location")
	RunbooksTag    = flag.String("runbooks-tag", EnvOrDefault("RUNBOOKS_TAG", "master"), "Runbooks tag to use")
)

func main() {
	flag.Parse()

	var cfg *rest.Config

	// Attempt incluster config first, if that fails, try getting from KUBECONFIG
	cfg, err := rest.InClusterConfig()
	if err != nil {
		log.Printf("Failed to get InCluster config, falling back to KUBECONFIG: %v", err)

		// We wont fall back to home directory, instead we expect this to be explictly set.
		// This is unusual behaviour, but this tool is typically expected to run incluster, so
		// setting KUBECONFIG implies a local development workflow
		kcfgPath := os.Getenv("KUBECONFIG")
		if kcfgPath == "" {
			log.Fatal("refusing to evaluate non-explict KUBECONFIG")
		}
		b, err := os.ReadFile(kcfgPath)
		if err != nil {
			log.Fatalf("Failed to read KUBECONFIG: %v", err)
		}
		ccfg, err := clientcmd.NewClientConfigFromBytes(b)
		if err != nil {
			log.Fatalf("failed to instantiate rest client from KUBECONFIG: %v", err)
		}
		cfg, err = ccfg.ClientConfig()
		if err != nil {
			log.Fatalf("failed to instantiate rest client from KUBECONFIG: %v", err)
		}
	}

	pClient := pOperator.NewForConfigOrDie(cfg)
	kClient := kubernetes.NewForConfigOrDie(cfg)

	// Dont need to overcomplicate this for now, lets just get something running
	var prov provider.Provider
	switch *Provider {
	case "K8SDYNAMIC_LOCAL":
		prov = provider.K8sDynamic{
			GenerationScriptPath: *k8sDynamic_GenerationScriptPath,
			RunbooksTag:          *RunbooksTag,
			RunbooksRemote:       *RunbooksRemote,
			RunbooksLocal:        *k8sDynamic_Runbooks_Local,
			OutputDir:            *k8sDynamic_OutputDir,
			Local:                true,
		}
		break
	case "K8SDYNAMIC_REMOTE":
		prov = provider.K8sDynamic{
			GenerationScriptPath: *k8sDynamic_GenerationScriptPath,
			RunbooksTag:          *RunbooksTag,
			RunbooksRemote:       *RunbooksRemote,
			RunbooksLocal:        *k8sDynamic_Runbooks_Local,
			OutputDir:            *k8sDynamic_OutputDir,
			Local:                false,

			// Stuff needed for the remote provider.
			ClientSet:    kClient,
			Namespace:    *TargetNamespace,
			CallbackAddr: *K8sDynamic_Remote_CallbackAddr,
		}
		break
	case "OBS_REGISTRY":
		break
	default:
		log.Fatalf("PROVIDER must be a valid choice from (K8SDYNAMIC_LOCAL, K8SDYNAMIC_REMOTE, OBS_REGISTRY)")
		break
	}

	v, err := prov.PromResources()
	if err != nil {
		panic(err)
	}

	rules, err := util.RuleFmtToK8sPrometheusRule(v, *TargetNamespace)
	if err != nil {
		panic(err)
	}

	// Apply all the rules
	// TODO delete rules that have been removed (.List())
	for _, r := range rules {
		_, err := pClient.MonitoringV1().PrometheusRules(*TargetNamespace).Apply(context.Background(), r, v1.ApplyOptions{
			FieldManager: "cell-observability-bootstrap",
		})
		if err != nil {
			log.Fatalf("%v", err)
		}
	}
}
